##############################################################################
# Project: Erlanger Urology Model
# Date: 9/23/2014
# Author: Sam Bussmann
# Description: Combine scores and get final ranking
# Notes: Used this 
##############################################################################


### Relative value from payer_type.R analysis
medicare <- .249-.179
medicaid <- .179-.179
commercial <- .265-.179
selfpay <- .015-.179

### Update dev.mod to have distance to urology office instead of distance to hospital
## Get distance to office
library(fields)
dist_out2<-rdist.earth(pm[,c("Longitude","Latitude")],hosploc1[,c("lng","lat")])
mindist_upd<-do.call(pmin,as.data.frame(dist_out2))
hist(mindist_upd)
## Get standarizing values for mindist
temp4<-read.table(file.path(getwd(),"standardized","MinDist"),header=T)
mindist_upd2<-(mindist_upd-temp4$mean)/temp4$stdev
## update dev.mod
dev.mod2<-dev.mod
dev.mod2[,which(dimnames(dev.mod2)[[2]]=="MinDist.std")]<-mindist_upd2

## create use prediction on updated dev.mod

pred.prop<-predict(cvnet1.p1,dev.mod2,#[cm$ClientName=="Claxton-Hepburn Medical Center",],
                   s = "lambda.min",type="response")

save(pred.prop,file="pred.prop.Rdata")
#load("pred.prop.Rdata")

## Update pred.payer to reflect last known payers from pt data

pred.payer.upd<-pred.payer[,,1]
pred.payer.upd[tmp2.payer=="Commercial",]<-matrix(c(1,0,0,0),
                                                            sum(tmp2.payer=="Commercial"),4,byrow=T)
pred.payer.upd[tmp2.payer=="Medicaid",]<-matrix(c(0,1,0,0),
                                                          sum(tmp2.payer=="Medicaid"),4,byrow=T)
pred.payer.upd[tmp2.payer=="Medicare",]<-matrix(c(0,0,1,0),
                                                          sum(tmp2.payer=="Medicare"),4,byrow=T)
pred.payer.upd[tmp2.payer=="Self Pay",]<-matrix(c(0,0,0,1),
                                                          sum(tmp2.payer=="Self Pay"),4,byrow=T)

### Calculatue expected value
expval<-(medicare*pred.payer.upd[,"Medicare"]+medicaid*pred.payer.upd[,"Medicaid"]+
           commercial*pred.payer.upd[,"Commercial"]+selfpay*pred.payer.upd[,"Self Pay"])

### Get CommunityPersonID, Expected Value, Propensity Score together

# tog<-data.frame(CommunityPersonID=pm$CommunityPersonID,
#                 CommunityHouseholdID=pm$CommunityHouseholdID,Zipcode=pm[,"zipcode"],
#                 prediction=as.numeric(pred.prop),expmargin=expval,predcost=as.numeric(exp(pred.cost)),
#                 score=as.numeric(pred.prop*expval*exp(pred.cost)))

tog<-data.frame(AddressID=pm$AddressID_orig[pm$TenantID==108],
                CommunityPersonID=pm$CommunityPersonID_orig[pm$TenantID==108],
                CommunityHouseholdID=pm$CommunityHouseholdID_orig[pm$TenantID==108],
                Zipcode=pm[pm$TenantID==108,"zipcode"],
                use_prediction=as.numeric(pred.prop), #charge_prediction=exp(as.numeric(pred.charge)),
                expmargin=expval, score=as.numeric(pred.prop*expval*100))  #*exp(pred.charge)))

save(tog,file="combined_score_df.RData")
#load("combined_score_df.RData")

hist(tog$use_prediction)
#hist(tog$charge_prediction)
hist(expval)
hist(tog$score,xlim=c(-.1,.5),breaks=200)

## look at score vs prediction

b<-10
qq1.1<-unique(quantile(tog$score, 
                       probs=seq.int(0,1, length.out=b+1)))
out1.1 <- cut(tog$score, breaks=qq1.1, include.lowest=TRUE, labels=as.character(c(10:1)))
qq1.2<-unique(quantile(tog$use_prediction, 
                       probs=seq.int(0,1, length.out=b+1)))
out1.2 <- cut(tog$use_prediction, breaks=qq1.2, include.lowest=TRUE, labels=as.character(c(10:1)))
plot(out1.2,out1.1,pch=".",xlab="Target Prediction",ylab="Final Score")

### Dedup by addressID,
### Also, consider the value of all residents at an address, but with decreasing value for each additional 
### positive scored resident. Use exponential decay.

# edecay<-function(x){
#   if (max(x) > 0) { t<-x[order(x,decreasing=T)]
#                     t[t<0]<-0
#                     tsum<-0
#                     for (i in 1:length(x)) tsum <- tsum + t[i]*exp(-(i-1))
#   } else 
#     tsum<-max(x)
#   return(tsum)
# }


addrby<-by(tog$score,tog$AddressID,
           
           function(x){
             if (max(x) > 0) { t<-x[order(x,decreasing=T)]
                               t[t<0]<-0
                               tsum<-0
                               for (i in 1:length(x)) tsum <- tsum + t[i]*exp(-(i-1))
             } else 
               tsum<-max(x)
             return(tsum)
           }
)

tog_2<-tog[order(tog$AddressID,tog$score,decreasing=T),]
tog_3<-tog_2[!duplicated(tog_2$AddressID),]
tog_4<-data.frame(tog_3[order(tog_3$AddressID),],score_upd=as.numeric(addrby))

plot(tog_4$score[1:20000],tog_4$score_upd[1:20000],pch=".")

## Check within address variability -- Is there much difference in the score of residents of the same address?
# 
# sdby<-by(tog_2$score,tog_2$AddressID,sd)
# hist(sdby,breaks=100,xlim=c(0,5))
# mean((sdby<1),na.rm=T)
# 
# minby<-by(tog_2$score,tog_2$AddressID,min)
# maxby<-by(tog_2$score,tog_2$AddressID,max)
# hist(maxby-minby,xlim=c(0,5),breaks=300)
# mean((maxby-minby==0),na.rm=T)

save(tog_4,file="final_score.RData")
#load("final_score.RData")

### Do some plotting

## Sum of score by decile
b<-10
qq<-unique(quantile(tog_4$score_upd, 
                    probs=seq.int(0,1, length.out=b+1)))
out <- cut(tog_4$score_upd, breaks=qq, include.lowest=TRUE, labels=as.character(c(10:1)))

a<-by(tog_4$score_upd,list(out),sum)
bb<-by(tog_4$score_upd,list(out),function(x) sum(!is.na(x)))

decile<-data.frame(decile=names(a),N=as.numeric(bb),sum_score=round(as.numeric(a),3),
                   ave_score=round(as.numeric(a)/as.numeric(bb),5),
                   lift=100*(round((as.numeric(a)/as.numeric(bb))/mean(tog_4$score_upd),3)-1))

library(gtools)
decile<-decile[mixedorder(decile$decile),]
decile

write.csv(decile,file="decile.csv",quote=F,row.names=F)
save(decile,file="decile.Rdata")

par(mfrow=c(1,1))
barplot(decile[,5],names.arg=as.character(1:10),col="lightblue",xlab="Decile",
        ylab="Percent Better than Average",
        main="Lift Chart",ylim=c(-100,400),yaxt="n")

# y-axis specifications
ticksy <-pretty(c(-100,0,100,200,300,400))
valuesy <- format(ticksy,big.mark=",",scientific=FALSE)
axis(2, at=ticksy, labels=paste0(sub("^\\s+", "",valuesy),"%"),cex.axis=0.8,las=1)


## EDA on final score

plot(tog_4$use_prediction,tog_4$expmargin,pch=".",
     main="Plot of Propensity against Expected Profit",
     xlab="Propensity Prediction",ylab="Expected Profit")
mean((tog_4$score_upd>0))

points(tog_4$use_prediction[out==10],tog_4$expmargin[out==10],pch=".",col="red")
points(tog_4$use_prediction[out==1],tog_4$expmargin[out==1],pch=".",col="blue")
points(tog_4$use_prediction[out==2],tog_4$expmargin[out==2],pch=".",col="green")


